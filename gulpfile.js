var
    gulp       = require('gulp'),
    less       = require('gulp-less'),
    path       = require('path'),
    concat     = require('gulp-concat'),
    debug      = require('gulp-debug'),
    cleanCSS   = require('gulp-clean-css'),
    template   = require('gulp-template-compile'),
    rename     = require('gulp-rename')
;

gulp.task('less', function () {
    var src = [
        './node_modules/bootstrap/less/bootstrap.less',
        './src/AppBundle/Resources/public/less/main.less'
    ];

    return gulp.src(src)
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(concat('all.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./web/css'))
        .pipe(debug({title: 'unicorn:'}))
    ;
});

gulp.task('font', function () {
    var src = [
        './node_modules/bootstrap/fonts/*',
    ];

    return gulp.src(src)
        .pipe(gulp.dest('./web/fonts'))
        .pipe(debug({title: 'unicorn:'}))
    ;
});

gulp.task('app', function () {
    var src = [
        './src/AppBundle/Resources/public/js/utils.js',
        './src/AppBundle/Resources/public/js/views/compiled/*',
        './src/AppBundle/Resources/public/js/views/*',
        './src/AppBundle/Resources/public/js/models/*',
        './src/AppBundle/Resources/public/js/app.js',
        './src/AppBundle/Resources/public/js/main.js'
    ];

    return gulp.src(src)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./web/js'))
        .pipe(debug({title: 'unicorn:'}))
    ;
});

gulp.task('template', function(){
    gulp.src('./src/AppBundle/Resources/public/js/views/templates/*')
        .pipe(rename(function(path){
            path.extname = '';
        }))
        .pipe(template({'namespace' : 'Templates'}))
        .pipe(gulp.dest('./src/AppBundle/Resources/public/js/views/compiled'))
});

gulp.task('watch', function() {
    gulp.watch(
        [
            './src/AppBundle/Resources/public/less/*.less',
            './src/AppBundle/Resources/public/js/*.js'
        ],
        ['less', 'app']
    );
});

gulp.task('build', function() {
    gulp.run('font');
    gulp.run('template');
    gulp.run('less');
    gulp.run('app');
});