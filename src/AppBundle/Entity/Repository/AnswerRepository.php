<?php

namespace AppBundle\Entity\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class AnswerRepository extends EntityRepository
{
    /**
     * @param $sid
     * @return ArrayCollection
     */
    public function findBySid($sid)
    {
        $answers = $this->createQueryBuilder('a')
            ->where('a.sid = ?1')
            ->setParameter(1, $sid)
            ->getQuery()
            ->getResult()
        ;

        return new ArrayCollection($answers);
    }
}