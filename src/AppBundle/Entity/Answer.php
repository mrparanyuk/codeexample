<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\AnswerRepository")
 * @ORM\Table(name="answers")
 */
class Answer
{
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $sid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $keyword;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $answer = null;

    /**
     * @ORM\Column(name="is_wrong", type="boolean", nullable=true)
     */
    protected $isWrong = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSid()
    {
        return $this->sid;
    }

    /**
     * @param mixed $sid
     */
    public function setSid($sid)
    {
        $this->sid = $sid;
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @param mixed $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return mixed
     */
    public function getIsWrong()
    {
        return $this->isWrong;
    }

    /**
     * @param mixed $isWrong
     */
    public function setIsWrong($isWrong)
    {
        $this->isWrong = $isWrong;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['keyword' => $this->getKeyword(),];
    }
}