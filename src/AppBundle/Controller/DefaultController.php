<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\StartType;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/question", name="question", options = { "expose" = true })
     */
    public function questionAction(Request $request)
    {
        if (! $request->isXmlHttpRequest() || ! $this->isStarted()) {
            throw $this->createNotFoundException();
        }

        $testManager = $this->get('app.test.manager');
        $data = json_decode($request->getContent(), true);

        // @todo rewrite for correct tests, may fixed in js - sendRequest, send as form
        if ($request->request->count()) {
            $data = $request->request->all();
        }

        if ($request->isMethod('POST') && $this->hasAnswer($data)) {
            $testManager->processAnswer($data);
        }
        $state = $testManager->next();

        return new JsonResponse($state);
    }

    /**
     * @return RedirectResponse
     * @Route("/restart", name="restart")
     */
    public function restartAction()
    {
        $this->get('app.test.manager')->restart();

        return $this->redirect($this->generateUrl('start'));
    }

    /**
     * @param Request $request
     * @return Response|RedirectResponse
     * @Route("/{react}", name="start", options = { "expose" = true }, requirements={"react":"^$|.+"})
     */
    public function indexAction(Request $request)
    {
        $testManager = $this->get('app.test.manager');

        if ($request->isXmlHttpRequest() && $request->isMethod('POST')) {
            $data = $request->request->all();
            $testManager->start($data['name']);

            return new JsonResponse(['success' => true]);
        }

        return $this->render('AppBundle::index.html.twig');
    }

    /**
     * @return boolean
     */
    protected function isStarted()
    {
        return $this->get('app.test.manager')->isStarted();
    }

    /**
     * @param $data
     * @return bool
     */
    protected function hasAnswer($data)
    {
        return isset($data['keyword']) && isset($data['answer']);
    }
}
