<?php

namespace AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\Answer;
use AppBundle\Entity\User;

class TestManager
{
    /**
     * @var array
     */
    protected $states = [
        'NOT_STARTED',
        'STARTED',
        'FAILED',
        'FINISHED'
    ];

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var JsonQuestionProvider
     */
    protected $jsonQuestionProvider;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var int
     */
    protected $wrongAnswersToFail;

    /**
     * @var int
     */
    protected $answersToFinish;

    /**
     * @var ArrayCollection
     */
    protected $answers = false;

    /**
     * @param Session $session
     * @param JsonQuestionProvider $jsonQuestionProvider
     * @param string $wrongAnswersToFail
     * @param string $answersToFinish
     */
    public function __construct(
        Session $session, JsonQuestionProvider $jsonQuestionProvider, EntityManager $em,
        $wrongAnswersToFail, $answersToFinish
    ) {
        $this->session = $session;
        $this->jsonQuestionProvider = $jsonQuestionProvider;
        $this->em = $em;
        $this->answersToFinish = $answersToFinish;
        $this->wrongAnswersToFail = $wrongAnswersToFail;
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        $state = $this->getState();

        return $state['type'] != 'NOT_STARTED';
    }

    /**
     * @return bool
     */
    public function isFailed()
    {
        $answers = $this->getAnswers();

        return $answers->filter(function($answer) {
            return $answer->getIsWrong();
        })->count() >= $this->wrongAnswersToFail;
    }

    /**
     * @return bool
     */
    public function isFinished()
    {
        $answers = $this->getAnswers();

        return ! $this->isFailed() && $answers->filter(function($answer) {
            return ! $answer->getIsWrong() && $answer->getAnswer();
        })->count() >= $this->answersToFinish;
    }

    /**
     * @param $name
     */
    public function start($name)
    {
        $this->session->set('user', $name);
    }

    /**
     * @return array
     */
    public function next()
    {
        $state = $this->getState();

        if (isset($state['stored']) && ! $state['stored']) {
            $answer = new Answer();
            $answer->setKeyword($state['question']['keyword']);
            $answer->setSid($this->session->getId());
            $answer->setCreatedAt(new \DateTime());
            $answer->setUpdatedAt(new \DateTime());
            $this->em->persist($answer);
            $this->em->flush();
            $this->session->set('answers', $state['question']['answers']);
        }

        if (in_array($state['type'], ['FAILED', 'FINISHED'])) {
            $user = new User();
            $user->setSid($this->session->getId());
            $user->setName($this->session->get('user'));
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());
            $user->setPoints($this->getRightAnswersCount());
            $this->em->persist($user);
            $this->em->flush();
        }

        return $state;
    }

    /**
     * @param $data
     * @return array
     */
    public function processAnswer($data)
    {
        $lastAnswer = $this->getAnswers()->last();

        if ($lastAnswer && $data['keyword'] != $lastAnswer->getKeyword()) {
            throw new NotFoundHttpException();
        }

        if ($lastAnswer) {
            $isWrong = $this->jsonQuestionProvider->isWrongAnswer($data['keyword'], $data['answer']);

            $repeatedQuestion = clone $lastAnswer;

            $lastAnswer->setIsWrong($isWrong);
            $lastAnswer->setAnswer($data['answer']);

            $this->em->persist($lastAnswer);

            if ($isWrong && ! $this->isFailed()) {
                $this->em->persist($repeatedQuestion);
            }

            $this->em->flush();
        }
    }

    /**
     * restart test for current user
     */
    public function restart()
    {
        $this->session->invalidate();
    }

    /**
     * @return array
     */
    protected function getState()
    {
        $user = $this->session->get('user', false);

        if (! $user) {
            return ['type' => 'NOT_STARTED'];
        }

        if ($this->isFailed()) {
            return ['type' => 'FAILED', 'points' => $this->getRightAnswersCount()];
        }

        if ($this->isFinished()) {
            return ['type' => 'FINISHED', 'points' => $this->getRightAnswersCount()];
        }

        $state = ['type' => 'STARTED'];
        $askedQuestion = $this->getAskedQuestion();

        if ($askedQuestion) {
            $state['question'] = $askedQuestion;
            $state['question']['answers'] = $this->session->get('answers');
            $state['stored'] = true;
        } else {
            $state['question'] = $this->getNextQuestion();
            $state['stored'] = false;
        }

        return $state;
    }

    /**
     * @return mixed
     */
    protected function getAskedQuestion()
    {
        $last = $this->getAnswers()->last();
        if ($last && ! $last->getAnswer()) {
            $last = $last->toArray();
            $last['is_wrong'] = $this->isLastWrongAnswers();

            return $last;
        }

        return false;
    }

    /**
     * @return array|mixed
     */
    protected function getNextQuestion()
    {
        $usedWords = $this->getAnswers()->map(function($answer){
            return $answer->getKeyword();
        });

        $question = $this->jsonQuestionProvider->getQuestion($usedWords);
        $question['is_wrong'] = false;

        return $question;
    }

    /**
     * @return ArrayCollection
     */
    protected function getAnswers()
    {
        $this->answers = $this->em->getRepository('AppBundle:Answer')->findBySid($this->session->getId());

        return $this->answers;
    }

    /**
     * @return int
     */
    protected function getRightAnswersCount()
    {
        $answers = $this->getAnswers();

        return $answers->filter(function($answer) {
            return ! $answer->getIsWrong();
        })->count();
    }

    /**
     * @return bool
     */
    protected function isLastWrongAnswers()
    {
        $answers = $this->getAnswers();

        if ($answers->count() < 2) {
            return false;
        }

        // array_values to reset keys
        list($one, $two) = array_values($answers->slice(-2));

        return $one->getIsWrong() && $one->getKeyword() == $two->getKeyword();
    }
}