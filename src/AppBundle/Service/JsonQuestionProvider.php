<?php

namespace AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

class JsonQuestionProvider implements QuestionProvider
{
    /**
     * @var mixed
     */
    protected $words;

    /**
     * @param string $wordsPath
     */
    public function __construct($wordsPath)
    {
        $this->words = json_decode(
            file_get_contents(getcwd() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $wordsPath),
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function isWrongAnswer($keyWord, $answer)
    {
        $words = $this->getWords();

        return $words[$keyWord] != $answer;
    }

    /**
     * @inheritdoc
     */
    public function getQuestion(ArrayCollection $usedKeywords)
    {
        $question = $this->getKeywordAndRightAnswer($usedKeywords);
        $words = $this->getWords();
        $question['answers'] = array_merge(
            $question['answers'],
            $this->getWrongAnswers($words[ $question['keyword'] ])
        );
        shuffle($question['answers']);

        return $question;
    }

    /**
     * @param ArrayCollection $usedKeywords
     * @return array
     */
    protected function getKeywordAndRightAnswer(ArrayCollection $usedKeywords)
    {
        $usedKeywordsKeys = $usedKeywords->isEmpty() ? [] : array_flip($usedKeywords->toArray());

        $unusedWords = array_diff_key($this->getWords(), $usedKeywordsKeys);
        $question = array_rand($unusedWords);

        return [
            'keyword' => $question,
            'answers' => [ $unusedWords[$question] ]
        ];
    }

    /**
     * @param string $keyword
     * @return array
     */
    protected function getWrongAnswers($keyword)
    {
        $unusedAnswers = array_diff($this->getWords(), [$keyword]);
        $keys = array_rand($unusedAnswers, 3);

        return array_map(function($key) use ($unusedAnswers) { return $unusedAnswers[$key]; }, $keys);
    }

    /**
     * @return mixed
     */
    protected function getWords()
    {
        return $this->words;
    }
}