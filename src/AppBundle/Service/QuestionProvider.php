<?php

namespace AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;

interface QuestionProvider
{
    /**
     * @param ArrayCollection $usedWords
     * @return mixed
     */
    public function getQuestion(ArrayCollection $usedWords);

    /**
     * @param $keyWord
     * @param $answer
     * @return mixed
     */
    public function isWrongAnswer($keyWord, $answer);
}