function App(config){
    this.config = config;
    this.view = new AppView();
    this.model = false;
}

App.prototype = {
    'run' : function() {
        var render   = this.view.render.bind(this.view),
            bind     = this.bindElements.bind(this),
            setModel = this.setModel.bind(this)
        ;
        this.getQuestion().then(setModel).then(render).then(bind).catch(function(error){
            console.error(error);
        });

    },
    'answer' : function(e) {
        var render = this.view.render.bind(this.view),
            bind   = this.bindElements.bind(this),
            setModel = this.setModel.bind(this)
        ;
        this.sendAnswer({
            'keyword' : this.model.question.keyword,
            'answer'  : e.target.innerText
        }).then(setModel).then(render).then(bind).catch(function(error){
            console.error(error);
        });
    },
    'getQuestion' : function() {
        return this.sendRequest(this.config.questionUrl, {'method' : 'GET'});
    },
    'sendAnswer' : function(data) {
        return this.sendRequest(this.config.questionUrl, {'method' : 'POST'}, JSON.stringify(data));
    },
    'sendRequest' : function(url, attr, data) {
        return new Promise(function(resolve, reject){
            var xhr = new XMLHttpRequest();

            xhr.open(attr.method, url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            xhr.send(data);

            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) return;

                if (xhr.status != 200) {
                    reject(xhr.status + ': ' + xhr.statusText);
                } else {
                    resolve(xhr.responseText);
                }
            }
        }).then(function(response){
            return JSON.parse(response);
        });
    },
    'bindElements' : function() {
        var buttons = document.getElementsByClassName('btn-answer'),
            answer
        ;

        for (var i in buttons) {
            if (buttons.hasOwnProperty(i)) {
                answer = this.answer.bind(this);
                buttons[i].onclick = answer;
            }
        }
    },
    'setModel' : function(data) {
        this.model = data;

        // to correct chain of Promise
        return data;
    }
};