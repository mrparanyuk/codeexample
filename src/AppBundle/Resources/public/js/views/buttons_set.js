function ButtonSetView(model){
    if (! model instanceof QuestionModel) {
        throw new Error('Please provide correct model');
    }
    View.call(this, model);
}

ButtonSetView.prototype = Object.create(View.prototype);
ButtonSetView.prototype.render = function(){
    var buttons = this.model.data.question.answers.map(function(answer){
        var button = new ButtonView();

        return button.render(answer);
    }).reduce(function(prevValue, currValue){
        return prevValue + currValue;
    });

    return Templates.buttons_set({'buttons' : buttons});
};
