function KeywordView(model){
    if (! model instanceof QuestionModel) {
        throw new Error('Please provide correct question');
    }
    this.model = model;
}

KeywordView.prototype = {
    'render' : function(){
        return Templates.keyword({'keyword' : this.model.data.question.keyword});
    }
};