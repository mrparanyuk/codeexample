(function() {
window["Templates"] = window["Templates"] || {};

window["Templates"]["button"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<a href="#" class="btn btn-default btn-lg btn-block btn-answer" role="button">' +
((__t = ( answer )) == null ? '' : __t) +
'</a>\n';

}
return __p
}})();