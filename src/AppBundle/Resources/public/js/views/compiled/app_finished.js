(function() {
window["Templates"] = window["Templates"] || {};

window["Templates"]["app_finished"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="container wrapperFinished">\n    <div class="text-center result-icon">\n        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>\n    </div>\n    <h1 class="text-center">Тест пройден</h1>\n    <h3 class="text-center">Вы набрали ' +
((__t = ( points )) == null ? '' : __t) +
' баллов</h3>\n    <div class="text-center">\n        <a class="btn btn-lg btn-success" href="/restart">\n            <span class="glyphicon glyphicon-retweet" aria-hidden="true"></span>\n            &nbsp;Начать заново\n        </a>\n    </div>\n    <!-- @todo рассказать друзьям -->\n</div>';

}
return __p
}})();