(function() {
window["Templates"] = window["Templates"] || {};

window["Templates"]["alert"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="alert alert-danger alert-animation" role="alert">Ошибка! Попробуй еще раз!</div>\n\n';

}
return __p
}})();