(function() {
window["Templates"] = window["Templates"] || {};

window["Templates"]["buttons_set"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '\n<div class="buttons-set">\n    ' +
((__t = ( buttons )) == null ? '' : __t) +
'\n</div>';

}
return __p
}})();