(function() {
window["Templates"] = window["Templates"] || {};

window["Templates"]["app"] = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<div class="container text-center" id="wrapperError"></div>\n<div class="container wrapperTest">\n    ' +
((__t = ( keyword )) == null ? '' : __t) +
'\n    ' +
((__t = ( buttons )) == null ? '' : __t) +
'\n</div>';

}
return __p
}})();