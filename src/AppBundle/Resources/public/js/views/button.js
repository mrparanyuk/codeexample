function ButtonView(){}

ButtonView.prototype = Object.create(View.prototype);
ButtonView.prototype.render = function(answer){
    return Templates.button({'answer' : answer});
};