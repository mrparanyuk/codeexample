function AppView(){}

AppView.prototype = {
    'el' : 'app',
    'render' : function(data) {
        if (data.type == 'STARTED' && data.question.is_wrong) {
            this.renderWrongAnswer();
        } else if (data.type == 'STARTED') {
            this.renderStarted(data);
        } else if (data.type == 'FAILED') {
            this.renderFailed(data);
        } else if (data.type == 'FINISHED') {
            this.renderFinished(data);
        }
    },
    'renderStarted' : function(data) {
        var model = new QuestionModel(data);
        var buttonSet = new ButtonSetView(model),
            keyword = new KeywordView(model)
        ;

        document.getElementById(this.el).innerHTML = '';
        document.getElementById(this.el).insertAdjacentHTML(
            'afterbegin', Templates.app({
                'buttons' : buttonSet.render(),
                'keyword' : keyword.render()
            })
        );
    },
    'renderWrongAnswer' : function() {
        var alert = new AlertView();

        document.getElementById(alert.el).innerHTML = '';
        document.getElementById(alert.el).insertAdjacentHTML('afterbegin', alert.render());
    },
    'renderFailed' : function(data) {
        document.getElementById(this.el).innerHTML = '';
        document.getElementById(this.el).insertAdjacentHTML(
            'afterbegin', Templates.app_failed({'points':data.points})
        );
    },
    'renderFinished' : function(data) {
        document.getElementById(this.el).innerHTML = '';
        document.getElementById(this.el).insertAdjacentHTML(
            'afterbegin', Templates.app_finished({'points':data.points})
        );
    }
};