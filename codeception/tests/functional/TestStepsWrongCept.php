<?php
use Step\Functional\StartSteps as StartTester;
$I = new StartTester($scenario);

$I->wantTo('Start test and fail three questions.');
$I->amOnRoute('start');
$I->seeCurrentRouteIs('start');
$I->canSeeResponseCodeIs(200);

$I->submitForm('#startForm', ['app_start[name]' => 'test']);
$I->seeCurrentRouteIs('test');
$I->canSeeResponseCodeIs(200);

$I->sendAjaxPostRequest('/question');

$I->doWrongTest($I, ['type' => 'STARTED', 'answer' => 'wrong']);
$I->doTest($I, ['type' => 'STARTED']);
$I->doWrongTest($I, ['type' => 'STARTED', 'answer' => 'wrong']);
$I->doTest($I, ['type' => 'STARTED']);
$I->doWrongTest($I, ['type' => 'STARTED', 'answer' => 'wrong']);

$content = json_decode($I->getContent(), true);
$I->assertTrue($content['type'] == 'FAILED');


