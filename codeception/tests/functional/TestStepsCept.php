<?php
use Step\Functional\StartSteps as StartTester;
$I = new StartTester($scenario);

$I->wantTo('Start test and pass four questions.');
$I->amOnRoute('start');
$I->seeCurrentRouteIs('start');
$I->canSeeResponseCodeIs(200);

$I->submitForm('#startForm', ['app_start[name]' => 'test']);
$I->seeCurrentRouteIs('test');
$I->canSeeResponseCodeIs(200);

$I->sendAjaxPostRequest('/question');


for ($i = 0; $i < 3; $i++) {
    $I->doTest($I, ['type' => 'STARTED']);
}

$I->doWrongTest($I, ['type' => 'STARTED', 'answer' => 'wrong']);
$content = json_decode($I->getContent(), true);
codecept_debug($content);
//$I->assertTrue($content['question']['is_wrong']);

$I->doTest($I, ['type' => 'STARTED']);

$content = json_decode($I->getContent(), true);
$I->assertTrue($content['type'] == 'FINISHED');


