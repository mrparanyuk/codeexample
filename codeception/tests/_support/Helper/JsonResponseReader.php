<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class JsonResponseReader extends \Codeception\Module
{
    public function getContent()
    {
        return $this->getModule('Symfony')->_getResponseContent();
    }
}
