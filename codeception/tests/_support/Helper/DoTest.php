<?php
namespace Helper;

use Codeception\Lib\ModuleContainer;
use Step\Functional\StartSteps;

class DoTest extends \Codeception\Module
{
    /**
     * @var array
     */
    protected $dictionary;

    public function __construct(ModuleContainer $moduleContainer, $config)
    {
        parent::__construct($moduleContainer, $config);
    }

    public function doTest(StartSteps $I, $expected)
    {
        $this->getDictionary();

        $I->canSeeResponseCodeIs(200);
        $content = json_decode($I->getContent(), true);
        $I->assertTrue($content['type'] == $expected['type']);
        $expectedRightAnswer = $this->dictionary[$content['question']['keyword']];
        $I->assertTrue(in_array($expectedRightAnswer, $content['question']['answers']));
        $I->sendAjaxPostRequest('/question', [
            'answer' => $expectedRightAnswer,
            'keyword' => $content['question']['keyword']
        ]);
    }

    public function doWrongTest(StartSteps $I, $expected)
    {
        $this->getDictionary();

        $I->canSeeResponseCodeIs(200);
        $content = json_decode($I->getContent(), true);
        $I->assertTrue($content['type'] == $expected['type']);
        $I->sendAjaxPostRequest('/question', [
            'answer' => $expected['answer'],
            'keyword' => $content['question']['keyword']
        ]);
    }

    /**
     * @return array|mixed
     * @throws \Codeception\Exception\ModuleException
     */
    protected function getDictionary()
    {
        if (! $this->dictionary) {
            $container = $this->getModule('Symfony')->grabService('kernel')->getContainer();
            $wordPath = $container->getParameter('words_path');
            $this->dictionary = json_decode(file_get_contents(getcwd() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $wordPath), true);
        }

        return $this->dictionary;
    }
}
