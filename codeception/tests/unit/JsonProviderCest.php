<?php

use Codeception\Util\Stub;

class JsonProviderCest
{
    public function _before(UnitTester $I)
    {
    }

    public function _after(UnitTester $I)
    {
    }

    /**
     * @dataprovider isRightAnswerProvider
     */
    public function testIsWrongAnswer(UnitTester $I, \Codeception\Example $example)
    {
        $result = $I->grabService('app.question.provider')->isWrongAnswer(
            $example['keyword'], $example['answer']
        );
        $example['expected'] ? $I->assertTrue($result) : $I->assertFalse($result);
    }

    /**
     * @dataprovider getQuestionProvider
     */
    public function testGetQuestion(UnitTester $I, \Codeception\Example $example)
    {
        $provider = Stub::make('AppBundle\Service\JsonQuestionProvider', ['getWords' => function() use ($example){
            return $example['words'];
        }]);
        $result = $provider->getQuestion(new \Doctrine\Common\Collections\ArrayCollection($example['used_words']));
        $I->assertTrue($result['keyword'] == $example['expected_keyword']);
        $I->assertTrue(count(array_unique($result['answers'])) == 4);
    }

    /**
     * @return array
     */
    protected function isRightAnswerProvider()
    {
        return [
            ['keyword' => "apple", 'answer' => "яблоко", 'expected' => 0],
            ['keyword' => "orange", 'answer' => "груша", 'expected' => 1],
        ];
    }

    /**
     * @return array
     */
    protected function getQuestionProvider()
    {
        return [
            [
                'words'            => ['test' => 'тест', 'english' => 'английский', 'tea' => 'чай', 'java' => 'чашка'],
                'used_words'       => ['test', 'english', 'tea'],
                'expected_keyword' => 'java'
            ],
        ];
    }
}
