const initialState = {
    'question' : {
        'answers' : [],
        'keyword' : ''
    },
    'type'    : 'NOT_STARTED'
};

export default function questions(state = initialState, action) {
    if (action.type == 'FETCH_QUESTION_SUCCESS') {
        return action.payload;
    } else if (action.type == 'SEND_ANSWER_SUCCESS') {
        return action.payload;
    } else if (action.type == 'FINISHED') {
        return action.payload;
    }

    return state;
}