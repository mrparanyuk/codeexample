import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import questions from './questions';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
    questions,
    form: formReducer,
    routing: routerReducer
});