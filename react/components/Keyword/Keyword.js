import React, { Component } from 'react';



class Keyword extends Component {
    render() {
        return (
            <h1 className="text-center">
                Выберите корректный перевод для &laquo;{this.props.keyword}&raquo;
            </h1>
        );
    }
}

export default Keyword;