import React from 'react';

const RenderField = ({input, meta}) => (
    <div className="input-row">
        <input className="form-control" {...input} type="text"/>
        {meta.touched && meta.error &&
        <span className="error">{meta.error}</span>}
    </div>
)

export default RenderField;
