import React, { Component } from 'react';

class Button extends Component {
    render() {
        return (
            <a href="#" onClick={this.props.onClick} className="btn btn-default btn-lg btn-block btn-answer" role="button">{this.props.value}</a>
        );
    }
}

export default Button;