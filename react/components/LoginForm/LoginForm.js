import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import RenderField from '../Form/Fields/Field.js';
import { startTest } from '../../actions/userActions.js';
import { hashHistory } from 'react-router';


class LoginForm extends Component
{
    render() {
        return (
            <form onSubmit={this.props.handleSubmit(submit.bind(this))}>
                <div className="form-group">
                    <label htmlFor="name">Ваше имя</label>
                    <Field name="name" component={RenderField} type="text"/>
                </div>
                <button type="submit">Начать тест</button>
            </form>
        );
    };
}

export default reduxForm({
    form: 'login' // a unique name for this form
})(LoginForm);