import React, { Component } from 'react';
import './App.less';

const App = (props) => {
    return (
        <div className="container">
            <div className="container text-center"></div>
            {props.children}
        </div>
    );
}

export default App;
