import Routing from './../common/importRouting.js';

const axios = require('axios');

exports.getQuestions = () => dispatch => {
    return axios.get(Routing.generate('question'), {'headers' : {'X-Requested-With': 'XMLHttpRequest'}}).then(function(response){
        dispatch({type: 'FETCH_QUESTION_SUCCESS', payload: response.data})
    })
};

exports.sendAnswer = (answer, keyword) => dispatch => {
    return axios.post(
        Routing.generate('question'),
        {'answer' : answer, 'keyword' : keyword},
        {'headers' : {'X-Requested-With': 'XMLHttpRequest'}}
    ).then(function(response){
        let type = response.type == 'STARTED' ? 'SEND_ANSWER_SUCCESS' : 'FINISHED';

        dispatch({type: type, payload: response.data})
    })
};

