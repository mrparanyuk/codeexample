import Routing from './../common/importRouting.js';

const axios = require('axios');

exports.startTest = (values) => {
    var payload = new FormData();
    payload.set('name', values.name);

    return axios.post(Routing.generate('start'), payload, {'headers' : {'X-Requested-With': 'XMLHttpRequest'}});
};

