import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import About from './components/About/About';
import Test from './components/Test/Test';
import Login from './containers/Login/Login';
import NotFound from './components/NotFound/NotFound';


const isAuth = (nextState, replace) => {
    if (localStorage.getItem('login') !== null) {
        replace('/test');
    }
};

const isAuthAlready = (nextState, replace) => {
    if (localStorage.getItem('login') === null) {
        replace('/login');
    }
};
<p></p>
const routes = (
    <div>
        <Route path='/' component={App} name='Home' onEnter={loadUser}>
            <IndexRoute onEnter={isAuthAlready} component={Test} />
            <Route onEnter={isAuth} >
                <Route path='/login' component={Login} name='Log In' />
            </Route>
            <Route onEnter={isAuthAlready}>
                <Route path="test" component={Test} />
                <Route path='about' component={About} name='About' />
            </Route>
        </Route>
        <Route path='*' component={NotFound} name="Not Found" />
    </div>
);

export default routes;